#include "wrapper.h"
 #include <sys/types.h>
 #include <unistd.h>
 #include <sys/wait.h>
 #include <string.h>
 #include <stdlib.h>
 #include <fcntl.h>
 #include <errno.h>
 
 #define MAX_TAB 100
 // #define MAX_URL_LENGTH 512
 extern int errno;
 
 /*
  * Name:                uri_entered_cb
  * Input arguments:     'entry'-address bar where the url was entered
  *                      'data'-auxiliary data sent along with the event
  * Output arguments:    none
  * Function:            When the user hits the enter after entering the url
  *                      in the address bar, 'activate' event is generated
  *                      for the Widget Entry, for which 'uri_entered_cb'
  *                      callback is called. Controller-tab captures this event
  *                      and sends the browsing request to the ROUTER (parent)
  *                      process.
  */
 void uri_entered_cb(GtkWidget* entry, gpointer data) {
 	if(data == NULL) {
 		return;
 	}
 	browser_window *b_window = (browser_window *)data;
 	// This channel has pipes to communicate with ROUTER.
 	comm_channel channel = b_window->channel;
 	// Get the tab index where the URL is to be rendered
 	int tab_index = query_tab_id_for_request(entry, data);
 	if(tab_index <= 0) {
 		fprintf(stderr, "Invalid tab index (%d).", tab_index);
 		return;
 	}
 	// Get the URL.
 	char * uri = get_entered_uri(entry);
 	// Append your code here
 	// ------------------------------------
 	// * Prepare a NEW_URI_ENTERED packet to send to ROUTER (parent) process.
 	// * Send the url and tab index to ROUTER
 	// ------------------------------------
 	printf("I am in uri entered call back with url %s \n", uri);
 
 	child_req_to_parent req;
 	req.type = NEW_URI_ENTERED;
 	child_request cr;
 	// cr.uri_req.uri = &uri;
 	strcpy(cr.uri_req.uri, uri);
 	printf("I am in uri entered call back checking return %s \n", cr.uri_req.uri);
 	cr.uri_req.render_in_tab = tab_index;
 	req.req = cr;
 	write(channel.child_to_parent_fd[1], &req, sizeof(child_req_to_parent));
 
 }
 
 /*
  * Name:                create_new_tab_cb
  * Input arguments:     'button' - whose click generated this callback
  *                      'data' - auxillary data passed along for handling
  *                      this event.
  * Output arguments:    none
  * Function:            This is the callback function for the 'create_new_tab'
  *                      event which is generated when the user clicks the '+'
  *                      button in the controller-tab. The controller-tab
  *                      redirects the request to the ROUTER (parent) process
  *                      which then creates a new child process for creating
  *                      and managing this new tab.
  */
 void create_new_tab_cb(GtkButton *button, gpointer data) {
 
 	if(data == NULL) {
 		return;
 	}
 	// This channel has pipes to communicate with ROUTER.
 	comm_channel channel = ((browser_window*)data)->channel;
 	printf("I am in create new tab call back\n");
 	// Append your code here.
 	// ------------------------------------
 	// * Send a CREATE_TAB message to ROUTER (parent) process.
 	child_req_to_parent req;
 	req.type = CREATE_TAB;
 	write(channel.child_to_parent_fd[1], &req, sizeof(child_req_to_parent)); 
 	// ------------------------------------
 }
 
 /*
  * Name:                url_rendering_process
  * Input arguments:     'tab_index': URL-RENDERING tab index
  *                      'channel': Includes pipes to communctaion with
  *                      Router process
  * Output arguments:    none
  * Function:            This function will make a URL-RENDRERING tab Note.
  *                      You need to use below functions to handle tab event.
  *                      1. process_all_gtk_events();
  *                      2. process_single_gtk_event();
 */
  int url_rendering_process(int tab_index, comm_channel *channel) {
  	// Don't forget to close pipe fds which are unused by this process
  	browser_window * b_window = NULL;

  	// Create url-rendering window
    fprintf(stderr, "render: %d\n\n", tab_index);
  	create_browser(URL_RENDERING_TAB, tab_index, G_CALLBACK(create_new_tab_cb), G_CALLBACK(uri_entered_cb), &b_window, channel);
  	child_req_to_parent req;
  	while (1) {
 		// Handle one gtk event, you don't need to change it nor understand what it does.
   		process_single_gtk_event();
   		// Poll message from ROUTER
   		// It is unnecessary to poll requests unstoppably, that will consume too much CPU time
   		// Sleep some time, e.g. 1 ms, and render CPU to other processes
   		usleep(1000);
   		// Append your code here
   		// child_req_to_parent * request = (child_req_to_parent*) malloc(sizeof(child_req_to_parent));
   		int nread;
   		// Try to read data sent from ROUTER
   		// If no data being read, go back to the while loop
   		if((nread = read(channel->parent_to_child_fd[0], &req, sizeof(req))) 
 					!= 520) {
 			if(errno == EAGAIN) {
				//pipe is empty, read did not "fail"
				// printf("nread is %d\n", nread);
 				continue;
 			} else {
 				perror("Failure during pipe read");				
				exit(-1);
 		   }
 		} else{
   		  printf("I am url rendering process %d and nread is %d\n", tab_index, nread);
    	// Otherwise, check message type:
   		printf("I am url redner process %d: I received message\n", tab_index);
    	//   * NEW_URI_ENTERED
    	//     ** call render_web_page_in_tab(req.req.uri_req.uri, b_window);
    	//   * TAB_KILLED
   		//     ** call process_all_gtk_events() to process all gtk events and jump out of the loop
   		//   * CREATE_TAB or unknown message type
   		//     ** print an error message and ignore it
   		// Handle read error, e.g. what if the ROUTER has quit unexpected?
   		switch (req.type) {
   				case NEW_URI_ENTERED:
   					// child_request child_req = request->req;
   					printf("%s\n", "I am url rendering process: I get new url");
   					render_web_page_in_tab(req.req.uri_req.uri, b_window);
   					break;
   				case TAB_KILLED:
   					printf("%s\n", "I am url rendering process: I will be killed");
   					process_all_gtk_events();
					free(channel);
					return 0;
   					break;
   				default:
   					perror("invalid request type in url rendering process");
 			  }
 		  }
 
 	}
 	return 0;
 }
 /*
  * Name:                controller_process
  * Input arguments:     'channel': Includes pipes to communctaion with
  *                      Router process
  * Output arguments:    none
  * Function:            This function will make a CONTROLLER window and
  *                      be blocked until the program terminates.
  */
 int controller_process(comm_channel *channel) {
 	// Do not need to change code in this function
 	printf("I am in controller process\n");
 	close(channel->child_to_parent_fd[0]);
 	close(channel->parent_to_child_fd[1]);
 	browser_window * b_window = NULL;
 	// Create controler window
 	create_browser(CONTROLLER_TAB, 0, G_CALLBACK(create_new_tab_cb), G_CALLBACK(uri_entered_cb), &b_window, channel);
 	show_browser();
 
 	return 0;
 }
 
 /*
  * Name:                router_process
  * Input arguments:     none
  * Output arguments:    none
  * Function:            This function implements the logic of ROUTER process.
  *                      It will first create the CONTROLLER process  and then
  *                      polling request messages from all ite child processes.
  *                      It does not need to call any gtk library function.
  */
 int router_process() {
 	comm_channel *channel[MAX_TAB];
 	int tab_pid_array[MAX_TAB] = {0}; 
  int guard_pid_array[MAX_TAB] = {0};
 	// You can use this array to save the pid
 	// of every child process that has be
 	// created. When a chile process receives
    // a TAB_KILLED message, you can call waitpid()
    // to check if the process is indeed completed.
    // This prevents the child process to become
    // Zombie process. Another usage of this array
    // is for bookkeeping which tab index has been
    // taken.
 	// Append your code here
 	// Prepare communication pipes with the CONTROLLER process
 	// Fork the CONTROLLER process
 	//   call controller_process() in the forked CONTROLLER process
 	// Don't forget to close pipe fds which are unused by this process
 	// Poll child processes' communication channels using non-blocking pipes.
 	// Before any other URL-RENDERING process is created, CONTROLLER process
 	// is the only child process. When one or more URL-RENDERING processes
 	// are created, you would also need to poll their communication pipe.
 	//   * sleep some time if no message received
 	//   * if message received, handle it:
 	//     ** CREATE_TAB:
 	//
 	//        Prepare communication pipes with the new URL-RENDERING process
 	//        Fork the new URL-RENDERING process
 	//
 	//     ** NEW_URI_ENTERED:
 	//
 	//        Send TAB_KILLED message to the URL-RENDERING process in which
 	//        the new url is going to be rendered
 	//
 	//     ** TAB_KILLED:
 	//
 	//        If the killed process is the CONTROLLER process
 	//        *** send TAB_KILLED messages to kill all the URL-RENDERING processes
 	//        *** call waitpid on every child URL-RENDERING processes
 	//        *** self exit
 	//
 	//        If the killed process is a URL-RENDERING process
 	//        *** send TAB_KILLED to the URL-RENDERING
 	//        *** call waitpid on every child URL-RENDERING processes
 	//        *** close pipes for that specific process
 	//        *** remove its pid from tab_pid_array[]
 	//

  // Set up pipe between router and controller
 	int flag1, nread;
    channel[0] = (comm_channel *) malloc(sizeof(comm_channel));
    
 	if (pipe(channel[0]->parent_to_child_fd) == -1 ||
 		pipe(channel[0]->child_to_parent_fd) == -1) {
 			perror("pipe setup failed");
 			exit(-1);
 		}
 		
 	flag1 = fcntl(channel[0]->parent_to_child_fd[0], F_GETFL, 0);
 	if (fcntl(channel[0]->parent_to_child_fd[0], F_SETFL, flag1 | O_NONBLOCK) == -1) {
 			perror("parents-child non-blocking setting failed");
 			exit(-1);
 	}
 
 	flag1 = fcntl(channel[0]->child_to_parent_fd[0], F_GETFL, 0);
 	if (fcntl(channel[0]->child_to_parent_fd[0], F_SETFL, flag1 | O_NONBLOCK) == -1) {
 			perror("child-parents non-blocking setting failed");
 			exit(-1);
 	}
 	

 	printf("start forking\n");
 	tab_pid_array[0] = fork();

 	// In router process
 	if (tab_pid_array[0] > 0) {
 		printf("fork controller successfully\n");
 		close((*channel[0]).parent_to_child_fd[0]);
 		close((*channel[0]).child_to_parent_fd[1]);
 		int n_process = 1;
 		int highest_session_process = 1;
 		
    // running infinitely
 		while(1) {

      // check every 1 ms
 			usleep(1000);
 			int i;
 			child_req_to_parent req;

      // check for message from child processes
 			for (i = 0; i < highest_session_process; i += 1) {
				
        // tab close and process exited
				if (tab_pid_array[i] == 0) {
					continue;	
				}
				
        // reach maximum number of tabs
				if (n_process == 99) {
					printf("you are about to reach the maximum number of tab!\n");
          // continue;
				}

        if (n_process >= 100){
          continue;
        }
				
				//  try to read from pipe
 				if((nread = read(channel[i]->child_to_parent_fd[0], &req, sizeof(req))) 
 					!= 520) {
 					if(errno == EAGAIN) {
						//pipe is empty, read did not "fail"
						// printf("nread is %d\n", nread);
 						continue;
 					} else {
 						perror("Failure during pipe read");
 				  }
 				    
 				} else {   // request received
 					printf("I am router process: %d messsage received\n", nread);

          // check request type
 					switch (req.type) {

            // create a mew tab 
 						case CREATE_TAB:
 							printf("I am router process trying to create tab\n");

              // initialize pipe
 							channel[n_process] = (comm_channel *) malloc(sizeof(comm_channel));
 							if (pipe((*channel[n_process]).parent_to_child_fd) == -1 ||
 								  pipe((*channel[n_process]).child_to_parent_fd) == -1) {
 									perror("pipe failed");
 							}
 
 							// setting up non-blocking 
 							flag1 = fcntl(channel[n_process]->parent_to_child_fd[0], F_GETFL, 0);
 							if (fcntl(channel[n_process]->parent_to_child_fd[0], F_SETFL, flag1 | O_NONBLOCK) == -1) {
 									perror("router to url render process non blocking failed");
 							}
 
 							flag1 = fcntl(channel[n_process]->child_to_parent_fd[0], F_GETFL, 0);
 							if (fcntl(channel[n_process]->child_to_parent_fd[0], F_SETFL, flag1 | O_NONBLOCK) == -1) {
 									perror("non-blocking setting failed");
 							}
 
 							// fork one child for new tab

 							tab_pid_array[n_process] = fork();
 							if (tab_pid_array[n_process] == 0) {   // in newly created url rendering process
                close(channel[n_process]->child_to_parent_fd[0]);
                close(channel[n_process]->parent_to_child_fd[1]);
                int guard = fork();
                if (guard == 0) { 
								  url_rendering_process(n_process, channel[n_process]);
								  exit(0);
                } else if (guard > 0) {
                  int status = 0;
                  waitpid(guard, &status, 0);
                  if (!WIFEXITED(status)){
                    child_req_to_parent kill_req;
                    kill_req.type = TAB_KILLED;
                    kill_req.req.killed_req.tab_index = n_process;
                    write(channel[n_process]->child_to_parent_fd[1], &kill_req, sizeof(child_req_to_parent));
                  }
                  exit(0);

                } else {
                  perror("fork guard process failed");
                }

							} else if (tab_pid_array[n_process] > 0) {   // in router process and set up non-blocking
 								   close(channel[n_process]->parent_to_child_fd[0]);
 								   close(channel[n_process]->child_to_parent_fd[1]);
							} else {   // failed to fork new process
  								perror("url process fork failed");
  						}   
              // still in router process check next avaible index number
  						while(tab_pid_array[n_process] != 0){
								n_process += 1;
							}
 							if (n_process > highest_session_process){
							  highest_session_process = n_process;	
							}
 							break;   // end of new tab creation 
 
            // render new url to existing tab
 						case NEW_URI_ENTERED:
 							printf("I am router process trying to render url to %d\n", req.req.uri_req.render_in_tab);
              // write to specified child process
              if (tab_pid_array[req.req.uri_req.render_in_tab] == 0){
                perror("invalid index number: tab do not exist!!!");
              }
 							if (write(channel[req.req.uri_req.render_in_tab]->parent_to_child_fd[1], &req, sizeof(child_req_to_parent)) == -1){
 								perror("write to url rendering failed !!!!");
 							}
  							break;   // end of rendering url
            
            // kill an existng process
  					case TAB_KILLED:
 							printf("I am router process trying to kill tab %d\n", req.req.killed_req.tab_index);
              // kill controller process
							if (req.req.killed_req.tab_index == 0){
								int j;
								printf("Killing controller process !!!\n");
								waitpid(tab_pid_array[0], NULL, 0);
                // killl all url rendering process (tabs) first
								for(j = highest_session_process-1; j >= 1; j--) {
									if (tab_pid_array[j] == 0){
										continue;
									}
									printf("%s %d\n", "writing kill message to tab", j);
									if (write(channel[j]->parent_to_child_fd[1], &req, sizeof(child_req_to_parent)) != -1){
										waitpid(tab_pid_array[j], NULL, 0);
										free(channel[j]);
										tab_pid_array[j] = 0;
										printf("%s %d\n", "returned from tab", j);
									} else{
										perror("write failed");
									}
								
								}
							  wait(NULL);
								exit(0);
							} else {   // kill non-controller process
								fprintf(stderr, "Here's the %d node\n", req.req.killed_req.tab_index);
								write(channel[req.req.killed_req.tab_index]->parent_to_child_fd[1], &req, sizeof(child_req_to_parent));
								fprintf(stderr, "Exit successfully\n");
								waitpid(tab_pid_array[req.req.killed_req.tab_index], NULL, 0);
								free(channel[req.req.killed_req.tab_index]);
								tab_pid_array[req.req.killed_req.tab_index] = 0;
								if (req.req.killed_req.tab_index < n_process){
									n_process = req.req.killed_req.tab_index;
								}
							}
							break;   // end of kill tab
 					}
 				}
 			}
 		}   // end of inifinite loop
 	}   // end of router process

  // in controller process
 	else if (tab_pid_array[0] == 0) {
 		controller_process(channel[0]);
 	}
 
  // failed to fork controller process
 	else{
 		perror("controller process fork failed");
 		exit(-1);
 	}

 	return 0;
 }
 
 int main() {
 	return router_process();
 }
 
