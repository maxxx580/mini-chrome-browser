# csci4061-project2
Login: TO DO   
Sate: 10/27/2016   
Name: Trevor Lecrone, Al Swenson, Zixiang Ma   
Id: 4968137(lecro007), 4962291(swens893), 4644999(maxxx580)   

# Assumption    
1. Router process will not be killed as the programming is running.    
2. When controller process is killed, all other processes(tabs) will be killed and results in the program to be stopped.    
3. Tabs are not allowed to be re-used. Once 100 tabs has been opened, program does not allow any more tab until being restarted.    
4. All system resources are properly freed and all processed eixts when router process exits. 
5. Program will ignore invalid information but program will exit whne encoutering fatal error. 